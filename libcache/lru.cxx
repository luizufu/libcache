#include <libcache/lru.hxx>
#include <numeric>

namespace cache
{
void lru::insert_new(uint32_t key, size_t i)
{
    _list.push_front({key, i});
    _map.insert({key, _list.begin()});
}

void lru::renew(ItemIterator it)
{
    _list.splice(_list.begin(), _list, it);
}

void lru::update_key(ItemIterator it, uint32_t key)
{
    _map.erase(it->first);
    _map[key] = it;
    it->first = key;
}

lru::lru(size_t max_pages, size_t page_size)
    : _data(max_pages * page_size)
    , _pool(max_pages)
    , _max_pages(max_pages)
    , _page_size(page_size)
{
    std::iota(_pool.rbegin(), _pool.rend(), 0);
}

auto lru::try_read(uint32_t key, std::byte* bytes) -> bool
{
    auto ref = _map.find(key);

    if(ref == _map.end())
    {
        return false;
    }

    renew(ref->second);

    size_t i = ref->second->second;
    std::copy(_data.begin() + i * _page_size,
              _data.begin() + (i + 1) * _page_size, bytes);

    ++_hits;

    return true;
}

void lru::write(uint32_t key, const std::byte* bytes)
{
    auto ref = _map.find(key);
    size_t i = 0;

    if(ref != _map.end())
    {
        i = ref->second->second;
        renew(ref->second);
    }
    else if(_list.size() + 1 > _max_pages)
    {
        auto last = std::prev(_list.end());
        i = last->second;

        update_key(last, key);
        renew(last);
    }
    else
    {
        i = _pool.back();
        _pool.pop_back();

        insert_new(key, i);
    }

    std::copy(bytes, bytes + _page_size, _data.begin() + i * _page_size);
}

void lru::clear()
{
    _list.clear();
    _map.clear();
    _pool.resize(_max_pages);
    std::iota(_pool.rbegin(), _pool.rend(), 0);
    reset_benchmark();
}

auto lru::size() const -> size_t
{
    return _list.size();
}

auto lru::hits() const -> uint64_t
{
    return _hits;
}

void lru::reset_benchmark() const
{
    _hits = 0;
}

} // namespace cache
