#pragma once

#include <bits/stdint-uintn.h>
#include <list>
#include <unordered_map>
#include <vector>

namespace cache
{
class lru
{
    using KeyValue = std::pair<uint32_t, size_t>;
    using ItemIterator = typename std::list<KeyValue>::iterator;

    std::vector<std::byte> _data;

    std::vector<size_t> _pool;
    std::list<KeyValue> _list;
    std::unordered_map<std::size_t, ItemIterator> _map;
    size_t _max_pages;
    size_t _page_size;

    mutable uint64_t _hits = 0;

    void insert_new(uint32_t key, size_t i);
    void renew(ItemIterator it);
    void update_key(ItemIterator it, uint32_t key);

public:
    explicit lru(size_t max_pages, size_t page_size);

    auto try_read(uint32_t key, std::byte* bytes) -> bool;
    void write(uint32_t key, const std::byte* bytes);
    void clear();
    auto size() const -> size_t;

    auto hits() const -> uint64_t;
    void reset_benchmark() const;
};

}  // namespace cache
