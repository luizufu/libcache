#include <bits/stdint-uintn.h>
#include <cassert>
#include <array>
#include <sstream>
#include <stdexcept>
#include <random>

#include <libcache/version.hxx>
#include <libcache/lru.hxx>

union page
{
    static const size_t size = 8;
    struct
    {
        uint32_t a;
        uint32_t b;
    };

    std::array<std::byte, size> bytes;
};

auto operator==(const page& p1, const page& p2) -> bool
{
    return p1.a == p2.a && p1.b == p2.b;
}


static auto prob() -> float
{
    static std::random_device r;
    static std::seed_seq seed {r(), r(), r(), r(), r(), r(), r(), r()};
    static std::mt19937 gen(seed);
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(gen);
}

auto random_pages(size_t n, size_t max_value) -> std::vector<page>
{
    std::vector<page> pages;

    while(n > 0)
    {
        pages.push_back({
            .a = static_cast<uint32_t>(prob() * max_value),
            .b = static_cast<uint32_t>(prob() * max_value) });

        --n;
    }

    return pages;
}

auto duplicate_n(std::vector<page>* pages, size_t n)
{
    for(size_t i = 0; i < n; ++i)
    {
        pages->insert(pages->end(), pages->begin(), pages->end());
    }

    std::shuffle(pages->begin(), pages->end(),
            std::mt19937 {std::random_device {}()});
}


auto main () -> int
{
    cache::lru c(5, page::size);

    page p1 = { .a = 1, .b = 5 };
    page p2 = { .a = 2, .b = 4 };
    page p3 = { .a = 3, .b = 3 };
    page p4 = { .a = 4, .b = 2 };
    page p5 = { .a = 5, .b = 1 };

    c.write(0, p1.bytes.data());
    c.write(1, p2.bytes.data());
    c.write(2, p3.bytes.data());
    c.write(3, p4.bytes.data());
    c.write(4, p5.bytes.data());


    page tmp = {};

    assert(c.try_read(0, tmp.bytes.data()));
    assert(tmp == p1);
    assert(c.try_read(1, tmp.bytes.data()));
    assert(tmp == p2);
    assert(c.try_read(2, tmp.bytes.data()));
    assert(tmp == p3);
    assert(c.try_read(3, tmp.bytes.data()));
    assert(tmp == p4);
    assert(c.try_read(4, tmp.bytes.data()));
    assert(tmp == p5);


    c.write(7, p1.bytes.data());
    c.write(8, p2.bytes.data());

    assert(!c.try_read(0, tmp.bytes.data()));
    assert(!c.try_read(1, tmp.bytes.data()));

    assert(c.try_read(7, tmp.bytes.data()));
    assert(tmp == p1);

    assert(c.try_read(8, tmp.bytes.data()));
    assert(tmp == p2);
}
